package mc.hund35.hub.loaders;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class items {

	
	public static ItemStack itemType1(Material m, int subid, int amount, Inventory inv, int slot){
		ItemStack is = new ItemStack(m,amount,(short)subid);
		inv.setItem(slot, is);		
		return is;
	}
	
	public static ItemStack itemType2(Material m, int subid, int amount, String name, Inventory inv, int slot){
		ItemStack is = new ItemStack(m,amount,(short)subid);
		is.setAmount(amount);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		is.setItemMeta(im);
		inv.setItem(slot, is);		
		return is;
	}
	public static ItemStack itemType3(Material m, int subid, int amount, String name, Inventory inv, int slot, String... lore){
		ItemStack is = new ItemStack(m,amount,(short)subid);
		is.setAmount(amount);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList(lore));
		im.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		is.setItemMeta(im);
		inv.setItem(slot, is);		
		return is;
	}
	
	
	
}
