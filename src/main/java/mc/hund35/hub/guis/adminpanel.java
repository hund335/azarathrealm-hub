package mc.hund35.hub.guis;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import mc.hund35.hub.main;
import mc.hund35.hub.loaders.items;

public class adminpanel {

	public static Inventory inv;
	public static Material m;
	
	public static Inventory AP(){
		
		inv = Bukkit.createInventory(null, 45, "§4§lADMIN PANEL");
		
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 0);
		items.itemType1(m.STAINED_GLASS_PANE, 5, 1, inv, 1);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 2);
		items.itemType1(m.STAINED_GLASS_PANE, 5, 1, inv, 3);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 4);
		items.itemType1(m.STAINED_GLASS_PANE, 5, 1, inv, 5);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 6);
		items.itemType1(m.STAINED_GLASS_PANE, 5, 1, inv, 7);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 8);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 36);
		items.itemType1(m.STAINED_GLASS_PANE, 5, 1, inv, 37);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 38);
		items.itemType1(m.STAINED_GLASS_PANE, 5, 1, inv, 39);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 40);
		items.itemType1(m.STAINED_GLASS_PANE, 5, 1, inv, 41);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 42);
		items.itemType1(m.STAINED_GLASS_PANE, 5, 1, inv, 43);
		items.itemType1(m.STAINED_GLASS_PANE, 13, 1, inv, 44);
		
		items.itemType2(m.WATCH, 0, 1, "§6§lSLÅ JOIN/LEAVE BESKDER FRA/TIL", inv, 11);
		items.itemType2(m.BARRIER, 0, 1, "§4§lSLÅ CHATTEN FRA/TIL", inv, 13);
		items.itemType2(m.GOLD_SWORD, 0, 1, "§c§lCLEAR CHATTEN", inv, 15);
		
		items.itemType2(m.STAINED_GLASS_PANE, 14, 1, "§c§lKOMMER SNART", inv, 29);
		items.itemType2(m.STAINED_GLASS_PANE, 14, 1, "§c§lKOMMER SNART", inv, 31);
		items.itemType2(m.STAINED_GLASS_PANE, 14, 1, "§c§lKOMMER SNART", inv, 33);
		
		
		return inv;
		
		
	}

	@EventHandler
	  public static void AdminP(InventoryClickEvent event)
	  {
	
		  if (event.getInventory().getTitle().equalsIgnoreCase("§4§lADMIN PANEL")){
			    Player p = (Player)event.getWhoClicked();
			    
				if(event.getCurrentItem() == null || event.getClickedInventory().getTitle() == null){
	        event.setCancelled(true);
	        return;
	        
	
				}else{
					event.setCancelled(true);
					  if (!event.getCurrentItem().hasItemMeta()) {
			                return;
			            }
			     
				      
				 if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§c§lKOMMER SNART")){
					event.setCancelled(true);
	        	p.sendMessage("§cKommer snart");
	        	
				 }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§c§lCLEAR CHATTEN")){
					 event.setCancelled(true);
					 p.closeInventory();
					for(int i = 1; i <  100; i += 1){
	                Bukkit.broadcastMessage("");
					}
				    Bukkit.broadcastMessage("§4§lCLEARCHAT §8§l» §cChatten er blevet ryddet af " + p.getName() + ".");
				    
				 }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§4§lSLÅ CHATTEN FRA/TIL")){
					 event.setCancelled(true);
				    File file = new File(main.instance.getDataFolder().getPath(), "settings.yml");
				      YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
				      p.closeInventory();
				      
				      if(config.getBoolean("Settings.Chat") == false){
				    	  config.set("Settings.Chat", true);
				    	  try {
							config.save(file);
						} catch (IOException e) {
							e.printStackTrace();
						}
				    	  Bukkit.broadcastMessage("§3§lCHAT §8§l» §7Chatten er blevet slået til af §b§l" + p.getName() + "§7.");
				      }else{
				    	  config.set("Settings.Chat", false); 
				    		try {
								config.save(file);
							} catch (IOException e) {
								e.printStackTrace();
							}
				    		  Bukkit.broadcastMessage("§4§lCHAT §8§l» §cChatten er blevet slået fra af " + p.getName() + ".");
				      }
					
	        }else{
	        	event.setCancelled(true);
	        }
	        }
	        	
	        }
	  
			
	  }
}


