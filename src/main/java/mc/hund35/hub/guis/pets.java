package mc.hund35.hub.guis;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import mc.hund35.hub.loaders.items;

public class pets implements Listener {

	public static Inventory inv;
	public static Material m;
	
	public static Inventory pets(Player p){
		
		inv = Bukkit.createInventory(null, 18, "§a§lFeatures - Kæledyr");
		
		if(p.hasPermission("hero.pets")){
	items.itemType3(m.MONSTER_EGG, 90, 1, "§a§lGris", inv, 0, "§7Tryk for at spawne dette kæledyr.");
	items.itemType3(m.MONSTER_EGG, 92, 1, "§a§lKo", inv, 1, "§7Tryk for at spawne dette kæledyr.");
	items.itemType3(m.MONSTER_EGG, 91, 1, "§a§lFår", inv, 2, "§7Tryk for at spawne dette kæledyr.");
	items.itemType3(m.MONSTER_EGG, 93, 1, "§a§lKylling", inv, 3, "§7Tryk for at spawne dette kæledyr.");
	
		
		
		}else{
			//p.sendMessage("§4§lFEJL §8§l» §dDenne funktion kræver: §a§lHERO §crank!");
			//p.sendMessage("§4§lFEJL §8§l» §dDet kan købes på: http://store.azarathrealm.com/");
			
			items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lGris", inv, 0, "§cKræver: §a§lHERO");
			items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lKo", inv, 1, "§cKræver: §a§lHERO");
			items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lFår", inv, 2, "§cKræver: §a§lHERO");
			items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lKylling", inv, 3, "§cKræver: §a§lHERO");
			
		}
		
		if(p.hasPermission("legend.pets")){
			items.itemType3(m.MONSTER_EGG, 95, 1, "§b§lUlv", inv, 4, "§7Tryk for at spawne dette kæledyr.");
			items.itemType3(m.MONSTER_EGG, 54, 1, "§b§lZombie", inv, 5, "§7Tryk for at spawne dette kæledyr.");
			items.itemType3(m.MONSTER_EGG, 51, 1, "§b§lSkeleton", inv, 6, "§7Tryk for at spawne dette kæledyr.");
			items.itemType3(m.MONSTER_EGG, 52, 1, "§b§lEdderkop", inv, 7, "§7Tryk for at spawne dette kæledyr.");
			
				
				
				}else{
					//p.sendMessage("§4§lFEJL §8§l» §dDenne funktion kræver: §a§lHERO §crank!");
					//p.sendMessage("§4§lFEJL §8§l» §dDet kan købes på: http://store.azarathrealm.com/");
					
					items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lUlv", inv, 4, "§cKræver: §b§lLEGEND");
					items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lZombie", inv, 5, "§cKræver: §b§lLEGEND");
					items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lSkeleton", inv, 6, "§cKræver: §b§lLEGEND");
					items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lEdderkop", inv, 7, "§cKræver: §b§lLEGEND");
					
				}
		if(p.hasPermission("king.pets")){
			items.itemType3(m.MONSTER_EGG, 57, 1, "§c§lZombie Gris", inv, 8, "§7Tryk for at spawne dette kæledyr.");
			items.itemType3(m.MONSTER_EGG, 50, 1, "§c§lCreeper", inv, 9, "§7Tryk for at spawne dette kæledyr.");
			items.itemType3(m.MONSTER_EGG, 61, 1, "§c§lBlaze", inv, 10, "§7Tryk for at spawne dette kæledyr.");
			items.itemType3(m.MONSTER_EGG, 58, 1, "§c§lWither Skeleton", inv, 11, "§7Tryk for at spawne dette kæledyr.");
			
				
				
				}else{
					//p.sendMessage("§4§lFEJL §8§l» §dDenne funktion kræver: §a§lHERO §crank!");
					//p.sendMessage("§4§lFEJL §8§l» §dDet kan købes på: http://store.azarathrealm.com/");
					
					items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lZombie Gris", inv, 8, "§cKræver: §c§lKING");
					items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lCreeper", inv, 9, "§cKræver: §c§lKING");
					items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lBlaze", inv, 10, "§cKræver: §c§lKING");
					items.itemType3(m.STAINED_GLASS_PANE, 14, 1, "§c§lWither Skeleton", inv, 11, "§cKræver: §c§lKING");
					
				}
		return inv;
	}

	@EventHandler
	  public void Pets(InventoryClickEvent event)
	  {
	
		  if (event.getInventory().getTitle().equalsIgnoreCase("§a§lFeatures - Kæledyr")){
			    Player p = (Player)event.getWhoClicked();
		
			    
				if(event.getCurrentItem() == null || event.getClickedInventory().getTitle() == null){
	        event.setCancelled(true);
	        return;
	        
	
				}else{
					event.setCancelled(true);
					  if (!event.getCurrentItem().hasItemMeta()) {
			                return;
			            }
				
					  
			     event.setCancelled(true);
				      
				 if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lKæledyr")){
					event.setCancelled(true);
					p.sendMessage("§4§lFEJL §8§l» §cKommer snart!");
					
				 }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§e§lFyrværkeri")){
					 event.setCancelled(true);
					 p.sendMessage("§4§lFEJL §8§l» §cKommer snart!");
				    
				 }else if(event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§d§lPartikler")){
					 event.setCancelled(true);
					 p.sendMessage("§4§lFEJL §8§l» §cKommer snart!");
					
	        }else{
	        	event.setCancelled(true);
	        }
	        }
	        	
	        }
	  
			
	  }
}


