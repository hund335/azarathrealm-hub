package mc.hund35.hub;

import java.awt.List;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import mc.hund35.hub.commands.setspawn;
import mc.hund35.hub.events.action;
import mc.hund35.hub.events.build;
import mc.hund35.hub.events.chat;
import mc.hund35.hub.events.commands;
import mc.hund35.hub.events.drop;
import mc.hund35.hub.events.falldamage;
import mc.hund35.hub.events.join;
import mc.hund35.hub.events.leave;
import mc.hund35.hub.events.rankupdate;
import mc.hund35.hub.events.secondhand;
import mc.hund35.hub.events.use;
import mc.hund35.hub.features.scoreboard;
import mc.hund35.hub.guis.choose;
import mc.hund35.hub.guis.features;
import mc.hund35.hub.guis.pets;


public class main
  extends JavaPlugin
  implements Listener, PluginMessageListener
{
	 public static main instance;
	 public ArrayList<Player>Players = new ArrayList();
	 
  public void onEnable()
  {
	  
//	ejer = sb.registerNewTeam("EJER");
	  
	  
	  instance = this;
    registerCommand();
    
    Bukkit.getPluginManager().registerEvents(this, this);
    Bukkit.getServer().getPluginManager().registerEvents(new join(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new action(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new chat(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new leave(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new use(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new choose(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new commands(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new drop(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new build(), this);
    Bukkit.getServer().getPluginManager().registerEvents(new falldamage(), this);
   Bukkit.getServer().getPluginManager().registerEvents(new secondhand(), this);
   Bukkit.getServer().getPluginManager().registerEvents(new features(), this);
   Bukkit.getServer().getPluginManager().registerEvents(new pets(), this);
   Bukkit.getServer().getPluginManager().registerEvents(new rankupdate(), this);
   //scoreboard.teamLoader();
   scoreboard.sbUpdate();
  Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
  Bukkit.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
  }
  private void registerCommand()
  {
  getCommand("setspawn").setExecutor(new setspawn());
  }
  
@Override
public void onPluginMessageReceived(String arg0, Player arg1, byte[] arg2) {
	
}
  
}
