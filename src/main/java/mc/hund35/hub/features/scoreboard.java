package mc.hund35.hub.features;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import mc.hund35.hub.main;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;



public class scoreboard {

	   public static Scoreboard sb = null;
	   public static Team ejer;
  	   public static Team dev;
  	   public static Team admin;
  	   public static Team mod;
  	   public static Team gamedesign;
  	   public static Team bygger;
  	   public static Team trainee;
  	   public static Team yt;
  	   public static Team king;
  	   public static Team legend;
  	   public static Team hero;
  	   
	
  	   public static int timer = 7;
  	   
	
	@SuppressWarnings("deprecation")
	public static void sbUpdate(){
		
	    Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(main.instance, new BukkitRunnable()
	    {
	      public void run()
	      {
	    	  
	        for (Player players : Bukkit.getServer().getOnlinePlayers()) {
	        	
	            sb = players.getScoreboard();
	        	
	        	if(timer == 7){
	             sb.getObjective("sb").setDisplayName("§3§lA§b§lzarath");
	             
	         	timer -= 1;
	        	}else if(timer == 6){
	        	sb.getObjective("sb").setDisplayName("§b§lA§3§lz§b§larath");
	        	timer -= 1;
	             }else if(timer == 5){
	        	sb.getObjective("sb").setDisplayName("§b§lAz§3§la§b§lrath");
	        	timer -= 1;
	        	}else if(timer == 4){
	        	sb.getObjective("sb").setDisplayName("§b§lAza§3§lr§b§lath");
	        	timer -= 1;
	        	}else if(timer == 3){
	        	sb.getObjective("sb").setDisplayName("§b§lAzar§3§la§b§lth");
	        	timer -= 1;
	        	}else if(timer == 2){
	        	sb.getObjective("sb").setDisplayName("§b§lAzara§3§lt§b§lh");
	        	timer -= 1;
	        	}else if(timer == 1){ 
	        	sb.getObjective("sb").setDisplayName("§b§lAzarat§3§lh");
	        	timer -= 1;
	        }else{
	        	timer = 7;
	        }
	        	}
	      }
	    }, 0L, 10L);
	  }
	
	 public static void sb(Player p)
	  {
	    sb = Bukkit.getScoreboardManager().getNewScoreboard();
	    Objective ob = sb.registerNewObjective("sb", "dummy");
	 
	  
          ejer = sb.registerNewTeam("EJER");
	      dev = sb.registerNewTeam("DEV");
	      admin = sb.registerNewTeam("ADMIN");
	      mod = sb.registerNewTeam("MOD");
	      gamedesign = sb.registerNewTeam("GAMEDESIGN");

	      bygger = sb.registerNewTeam("BYGGER");
	      trainee = sb.registerNewTeam("TRAINEE");
	      yt = sb.registerNewTeam("YOUTUBER");
	      king = sb.registerNewTeam("KING");
	      legend = sb.registerNewTeam("LEGEND");
	      hero = sb.registerNewTeam("HERO");
      	    
      	       ejer.setPrefix("§d§lEJER §f");
      		   dev.setPrefix("§5§lDEV §f");
      		   admin.setPrefix("§4§lADMIN §f");
      		   mod.setPrefix("§6§lMOD §f");
      		   
      		   gamedesign.setPrefix("§9§lGD §f");
      		   bygger.setPrefix("§9§lBYGGER §f");
      		   trainee.setPrefix("§e§lTRAINEE §f");
      		   yt.setPrefix("§c§lY§f§lT §f");
      		   king.setPrefix("§c§lKING §f");
      		   legend.setPrefix("§b§lLEGEND §f");
      		   hero.setPrefix("§a§lHERO §f");
      		   
      		
      		 
      		   for(Player players : Bukkit.getServer().getOnlinePlayers()){
      			  PermissionUser user = PermissionsEx.getUser(p);
				  PermissionUser users = PermissionsEx.getUser(players);
		 
		    if(users.inGroup("Ejer")){   
	        	 ejer.addPlayer(players);
	    	}else if(users.inGroup("Dev")){
	    		dev.addPlayer(players);
	    	}else if(users.inGroup("Admin")){
	    		admin.addPlayer(players);
	    	}else if(users.inGroup("Mod")){
	    		mod.addPlayer(players);
	    	}else if(users.inGroup("GameDesign")){
	    		gamedesign.addPlayer(players);
	    	}else if(users.inGroup("Bygger")){
	    		bygger.addPlayer(players);
	    	}else if(users.inGroup("Trainee")){
	    		trainee.addPlayer(players);
	    	}else if(users.inGroup("Youtuber")){
	    		yt.addPlayer(players);
	    	
	    	}else if(users.inGroup("King")){
	    		king.addPlayer(players);
	         }else if(users.inGroup("Legend")){
	        	 legend.addPlayer(players);
	         }else if(users.inGroup("Hero")){
	        	 hero.addPlayer(players);
	    	  }else{
	    		  
	    	  }
	 
	 
	    
	    ob.setDisplaySlot(DisplaySlot.SIDEBAR);
	   
	 
	    
	    ob.setDisplayName("§b§lAzarath");
	    
	    ob.getScore("§0").setScore(13);
	    ob.getScore("  §3§lGLOBALT").setScore(12);
	    


	 if(user.inGroup("Ejer")){   
	   ob.getScore("  §fRank: §dEJER").setScore(10);    	 
   	}else if(user.inGroup("Dev")){
   	  ob.getScore("  §fRank: §5DEV").setScore(10);
   	}else if(user.inGroup("Admin")){
   	  ob.getScore("  §fRank: §4ADMIN").setScore(10);
   	}else if(user.inGroup("Mod")){
   	  ob.getScore("  §fRank: §6MOD").setScore(10);
   	}else if(user.inGroup("GameDesign")){
   	  ob.getScore("  §fRank: §9GAME DESIGN").setScore(10);
   	}else if(user.inGroup("Bygger")){
   	  ob.getScore("  §fRank: §9BYGGER").setScore(10);
   	}else if(user.inGroup("Trainee")){
   	  ob.getScore("  §fRank: §eTRAINEE").setScore(10);
   	}else if(user.inGroup("Youtuber")){
   	  ob.getScore("  §fRank: §cY§fT").setScore(10);
   	}else if(user.inGroup("King")){
   	  ob.getScore("  §fRank: §cKING").setScore(10);
    }else if(user.inGroup("Legend")){
      ob.getScore("  §fRank: §bLEGEND").setScore(10);
    }else if(user.inGroup("Hero")){
      ob.getScore("  §fRank: §aHERO").setScore(10);
   	}else{
   	  ob.getScore("  §fRank: §7Intet").setScore(10); 
   	  }
	 
	 

	    
	    ob.getScore("  §fOnline: §bKommer").setScore(9); 
	    ob.getScore("§d").setScore(8);
	    ob.getScore("  §3§lHUB").setScore(7);
	    ob.getScore("  §fKæledyr: §cIntet").setScore(6);
	    ob.getScore("  §fPartikel: §cIntet").setScore(5);
	    ob.getScore("§4").setScore(4);
	    ob.getScore("  §3§lRPG").setScore(3);
	    ob.getScore("  §fClass: §bKommer").setScore(2);
	    ob.getScore("  §fLevel: §bKommer").setScore(1);
	    ob.getScore("  §fGuld: §bKommer").setScore(0);
	    ob.getScore("§5").setScore(-1);
	    ob.getScore("      §bazarathrealm.com").setScore(-2);
	    
	    p.setScoreboard(sb);
	  
	  }
}
	
}