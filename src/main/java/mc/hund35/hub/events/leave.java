package mc.hund35.hub.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import mc.hund35.hub.main;
import mc.hund35.hub.messages;

public class leave implements Listener{
	
	@EventHandler
	  public void playerLeave(PlayerQuitEvent event){
		Player p = (Player)event.getPlayer();
		messages.leaveMessage(event);
	}
	  

}
