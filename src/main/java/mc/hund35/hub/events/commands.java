package mc.hund35.hub.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class commands implements Listener {

	 @EventHandler
	    public void PlayerCommand(PlayerCommandPreprocessEvent event) {
		 Player p = (Player)event.getPlayer();
		  PermissionUser user = PermissionsEx.getUser(p);
		  
		 if(user.inGroup("Mod") || user.inGroup("Admin") || user.inGroup("Dev") || user.inGroup("Ejer")){
			
		 }else{
			 p.sendMessage("§b§lHUB §8§l» §cAlle kommandoer er slået fra på hubben!");
			 event.setCancelled(true);
		 }
		 
	 }
	 
	 @EventHandler
	 public void Auto(PlayerChatTabCompleteEvent event){
		 event.getTabCompletions().clear();
	 }
	
}
