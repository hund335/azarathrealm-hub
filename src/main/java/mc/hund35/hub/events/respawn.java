package mc.hund35.hub.events;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import mc.hund35.hub.main;

public class respawn implements Listener {

	@EventHandler
	public void spawn(PlayerRespawnEvent event){
		 File file2 = new File(main.instance.getDataFolder().getPath(), "spawn.yml");
	        YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
	        World world = Bukkit.getServer().getWorld(config2.getString("Spawn.World"));	       
		event.setRespawnLocation(new Location(world, config2.getInt("Spawn.X"), config2.getInt("Spawn.Y"), config2.getInt("Spawn.Z"), config2.getInt("Spawn.Yaw"), config2.getInt("Spawn.Pitch")));
		
	}
	
}
