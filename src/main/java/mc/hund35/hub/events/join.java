package mc.hund35.hub.events;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import mc.hund35.hub.main;
import mc.hund35.hub.messages;
import mc.hund35.hub.features.scoreboard;
import mc.hund35.hub.loaders.blacklist;
import mc.hund35.hub.loaders.load;

public class join implements Listener{
	
	@EventHandler
	  public void playerJoin(PlayerJoinEvent event){
		Player p = (Player)event.getPlayer();
	   
for(Player players : Bukkit.getServer().getOnlinePlayers()){
		scoreboard.sb(players);
}
		load.getItems(p);
		blacklist.getBlacklist(p);
		messages.joinMessage(event);
        File file2 = new File(main.instance.getDataFolder().getPath(), "spawn.yml");
        YamlConfiguration config2 = YamlConfiguration.loadConfiguration(file2);
        World world = Bukkit.getServer().getWorld(config2.getString("Spawn.World"));
        p.teleport(new Location(world, config2.getInt("Spawn.X"), config2.getInt("Spawn.Y"), config2.getInt("Spawn.Z"), config2.getInt("Spawn.Yaw"), config2.getInt("Spawn.Pitch")));
	}
	  

}
