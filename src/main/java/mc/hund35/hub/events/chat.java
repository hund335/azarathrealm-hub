package mc.hund35.hub.events;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import mc.hund35.hub.main;
import mc.hund35.hub.loaders.text;
import net.md_5.bungee.api.ChatColor;
import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class chat implements Listener {

	@EventHandler
	public void Prefix(AsyncPlayerChatEvent event){
	Player p = (Player)event.getPlayer();

	 String Ejer = "§8§l§m-------------§r §d§lEJER §8§l§m-------------§r\n§fDenne spiller ejer hele serveren.\n§8(§cDette rank kan ikke skaffes på nogen måder§8)";
	 String Dev = "§8§l§m-------------§r §5§lDEV §8§l§m-------------§r\n§fDenne spiller er udvikler på serveren,\n§fhvilket betyder spilleren laver programmerings arbejde for os.\n§8(§eDette rank kan kun skaffes ved at blive ansat af ledelsen§8)";
	 String Admin = "§8§l§m-------------§r §4§lADMIN §8§l§m-------------§r\n§fDenne spiller er admin på serveren,\n§fhvilket betyder spilleren administrere og tager,\n§fvigtige beslutninger når en Ejer ikke er til stede.\n§8(§eDette rank kan kun skaffes ved at være trusted af en ejer§8)";
	 String Mod = "§8§l§m-------------§r §6§lMOD §8§l§m-------------§r\n§fDenne spiller er moderator på serveren,\n§fhvilket betyder spilleren primært står for at moderere chatten,\n§fsvarer på reports, sørger for at straf hackere og\n§fandet in-game moderation arbejde.\n§8(§eDette rank kan kun skaffes ved at blive promoted fra trainee§8)";
	 String GD = "§8§l§m-------------§r §9§lGAME DESIGN §8§l§m-------------§r\n§fDenne spiller er game designer på serveren,\n§fhvilket betyder spilleren står for at design quests osv.\n§8(§eDette rank kan kun skaffes ved at blive valgt af ledelsen§8)";
	 String Bygger = "§8§l§m-------------§r §9§lBYGGER §8§l§m-------------§r\n§fDenne spiller er bygger på serveren,\n§fhvilket betyder spilleren står for at bygge de ting,\n§fsom game design teamet har valgt skal tilføjes.\n§8(§eDette rank kan kun skaffes ved at ansøge på forum§8)";
	 String Trainee = "§8§l§m-------------§r §e§lTRAINEE §8§l§m-------------§r\n§fDenne spiller er trainee på serveren,\n§fhvilket betyder spilleren er under lære som moderator,\n§fdet betyder derfor spilleren kun står for at moderere chatten.\n§8(§eDette rank kan kun skaffes ved at blive accepteret som trainee på forummet§8)";
	 String YT = "§8§l§m-------------§r §c§lYOU§f§lTUBER §8§l§m-------------§r\n§fDenne spiller er youtuber på serveren,\n§fhvilket betyder spilleren laver videoer fra serveren af.\n§8(§eDette rank kan kun skaffes ved at ansøge på forum\n§eeller blive kontaktet af os privat§8)";
	 String King = "§8§l§m-------------§r §c§lKING §8§l§m-------------§r\n§fDenne spiller er king på serveren,\n§fhvilket lige pt. er den dyreste donator rank\n§fog hjælper os derfor med at holde serveren i live.\n§8(§aDette rank kan kun skaffes ved at købe det\n§ai vores shop, /upgrade for mere info.§8)";
	 String Legend = "§8§l§m-------------§r §b§lLEGEND §8§l§m-------------§r\n§fDenne spiller er legend på serveren,\n§fhvilket lige pt. er den næstdyreste donator rank\n§fog hjælper os derfor med at holde serveren i live.\n§8(§aDette rank kan kun skaffes ved at købe det\n§ai vores shop, /upgrade for mere info.§8)";
	 String Hero = "§8§l§m-------------§r §a§lHERO §8§l§m-------------§r\n§fDenne spiller er hero på serveren,\n§fhvilket lige pt. er den billigste donator rank\n§fog hjælper os derfor med at holde serveren i live.\n§8(§aDette rank kan kun skaffes ved at købe det\n§ai vores shop, /upgrade for mere info.§8)";
	
	  	if(event.isCancelled() == false){
	  		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"));
			   int hours = cal.get(Calendar.HOUR) + 1;
			   int minutes = cal.get(Calendar.MINUTE);
			   int seconds = cal.get(Calendar.SECOND);	
		

		      for(Player players : Bukkit.getOnlinePlayers()){
		      
		      PermissionUser user = PermissionsEx.getUser(p);
		  	Date date = new Date();
		  	PermissionManager pex = PermissionsEx.getPermissionManager();
		 
		  	 if(user.inGroup("Ejer")){   

		 		players.spigot().sendMessage(text.HoverMessage("§d§lEJER ", Ejer), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §f"), text.Prefix(event.getMessage().replace("&", "§").replace("%", "P"), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		 
		 		event.setCancelled(true);
		 	}else if(user.inGroup("Dev")){
		 		players.spigot().sendMessage(text.HoverMessage("§5§lDEV ", Dev), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §f"), text.Prefix(event.getMessage().replace("&", "§").replace("%", "P"), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);
		 	}else if(user.inGroup("Admin")){
		 		players.spigot().sendMessage(text.HoverMessage("§4§lADMIN ", Admin), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §f"), text.Prefix(event.getMessage().replace("&", "§").replace("%", "P"), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);
		 	}else if(user.inGroup("Mod")){
		 		players.spigot().sendMessage(text.HoverMessage("§6§lMOD ", Mod), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);
		 	}else if(user.inGroup("GameDesign")){
		 		players.spigot().sendMessage(text.HoverMessage("§9§lGAME DESIGN ", GD), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);
		 	}else if(user.inGroup("Bygger")){
		 		players.spigot().sendMessage(text.HoverMessage("§9§lBYGGER ", Bygger), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);
		 	}else if(user.inGroup("Trainee")){
		 		players.spigot().sendMessage(text.HoverMessage("§e§lTRAINEE ", Trainee), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);
		 	} else if(user.inGroup("Youtuber")){
		 		players.spigot().sendMessage(text.HoverMessage("§c§lYOU§f§lTUBER ", YT), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);     
		 	} else if(user.inGroup("King")){
		 		players.spigot().sendMessage(text.HoverMessage("§c§lKING ", King), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);
		      }else if(user.inGroup("Legend")){
		 	  	players.spigot().sendMessage(text.HoverMessage("§b§lLEGEND ", Legend), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);
		      }else if(user.inGroup("Hero")){
		 		players.spigot().sendMessage(text.HoverMessage("§a§lHERO ", Hero), text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.WHITE));
		 		event.setCancelled(true);		
		 	  }else{

		 		players.spigot().sendMessage(text.HoverMessage("§f" + p.getName(), "§dSej spiller"), text.Message(" §8§l» §7"), text.Prefix(event.getMessage(), "Beskeden blev sendt kl: §f§l" + hours + ":" + minutes + ":" + seconds, ChatColor.GRAY));
		 	
		 		event.setCancelled(true);
		 			      }
		 	  
		 	      }
	  	}
	
	}
}
