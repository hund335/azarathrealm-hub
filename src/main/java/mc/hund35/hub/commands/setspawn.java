package mc.hund35.hub.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import mc.hund35.hub.main;


public class setspawn
  implements CommandExecutor, Listener
{
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§b§lHUB §8§l§ §cDenne kommando kan kun bruges in-game!");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("setspawn")) {
      if (p.hasPermission("setspawn.use"))
      {
        if (args.length == 0)
        {
          File file = new File(main.instance.getDataFolder().getPath(), "spawn.yml");
          YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
          config.set("Spawn.World", p.getLocation().getWorld().getName());
          config.set("Spawn.X", Integer.valueOf(p.getLocation().getBlockX()));
          config.set("Spawn.Y", Integer.valueOf(p.getLocation().getBlockY()));
          config.set("Spawn.Z", Integer.valueOf(p.getLocation().getBlockZ()));
          config.set("Spawn.Pitch", Float.valueOf(p.getLocation().getPitch()));
          config.set("Spawn.Yaw", Float.valueOf(p.getLocation().getYaw()));
          p.sendMessage("§b§lHUB §8§l» §7Du har nu sat spawn locationen.");
          try
          {
            config.save(file);
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }
        }
      }
      else {
        p.sendMessage("§4§lHUB §8§l» §cDu har ikke tillades til denne kommando!");
      }
    }
    return false;
  }
}
